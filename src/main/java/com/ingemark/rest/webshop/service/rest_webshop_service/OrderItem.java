package com.ingemark.rest.webshop.service.rest_webshop_service;

import javax.persistence.Entity;

public class OrderItem {
    private Long order_id;
    private Long product_id;
    private int quantity;

    public OrderItem(Long order_id, int quantity) {
        this(order_id, null, quantity);
    }
    public OrderItem(Long order_id, Long product_id) {
        this(order_id, product_id, 0);
    }
    public OrderItem(Long order_id, Long product_id, int quantity) {
        this.order_id = order_id;
        this.product_id = product_id;
        this.quantity = quantity;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

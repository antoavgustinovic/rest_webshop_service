package com.ingemark.rest.webshop.service.rest_webshop_service.repository;

import com.ingemark.rest.webshop.service.rest_webshop_service.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}

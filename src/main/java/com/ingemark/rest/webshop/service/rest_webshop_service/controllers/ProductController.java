package com.ingemark.rest.webshop.service.rest_webshop_service.controllers;

import com.ingemark.rest.webshop.service.rest_webshop_service.model.Product;
import com.ingemark.rest.webshop.service.rest_webshop_service.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Product getProduct(@PathVariable Long id) throws Exception {
        return productService.getProduct(id);
    }

    @PostMapping(value = "/products")
    public void add(@RequestBody @Valid Product product) throws Exception {
        productService.addProduct(product);
    }

    @PutMapping(value = "/products/{id}")
    public void update(@RequestBody @Valid Product product) throws Exception {
        productService.updateProduct(product);
    }

    @DeleteMapping(value = "/products/{id}")
    public void delete(@PathVariable Long id) throws Exception {
        productService.deleteProduct(id);
    }
}

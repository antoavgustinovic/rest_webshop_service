package com.ingemark.rest.webshop.service.rest_webshop_service.enumeration;

public enum OrderStatus {
    DRAFT, SUBMITTED
}

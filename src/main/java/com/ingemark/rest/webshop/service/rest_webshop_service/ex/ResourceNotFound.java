package com.ingemark.rest.webshop.service.rest_webshop_service.ex;

import java.util.function.Supplier;

public class ResourceNotFound extends Exception{

    public ResourceNotFound(String message) {
        super(message);
    }

    public ResourceNotFound() {
    }

    public static Supplier<ResourceNotFound> supplier(Long id) {
        return () -> new ResourceNotFound(String.format("Product with id '%s' not found", id));
    }
}

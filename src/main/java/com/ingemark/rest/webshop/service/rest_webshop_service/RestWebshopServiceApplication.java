package com.ingemark.rest.webshop.service.rest_webshop_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestWebshopServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestWebshopServiceApplication.class, args);
	}

}

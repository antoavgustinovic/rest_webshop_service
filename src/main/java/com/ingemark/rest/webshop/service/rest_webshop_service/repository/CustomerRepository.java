package com.ingemark.rest.webshop.service.rest_webshop_service.repository;

import com.ingemark.rest.webshop.service.rest_webshop_service.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}

package com.ingemark.rest.webshop.service.rest_webshop_service.ex;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFound.class)
    @ResponseBody
    public ResourceNotFound handleResourceNotFound(ResourceNotFound ex){
        logger.debug(ex.getMessage(), ex);
        return ex;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ResourceAlreadyExists.class})
    @ResponseBody
    public ResourceAlreadyExists handleBadRequest(ResourceAlreadyExists ex){
        logger.debug(ex.getMessage(), ex);
        return ex;
    }
}

package com.ingemark.rest.webshop.service.rest_webshop_service.repository;

import com.ingemark.rest.webshop.service.rest_webshop_service.model.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {

    @Query("SELECT o.customer, o.totalPriceHrk, o.totalPriceEur, o.status, c.firstName, c.lastName, c.email FROM " +
            "Order o JOIN Customer c " +
            "ON o.customer.id = c.id")
    List<Order> fetchOrdersFromCustomer();
}

package com.ingemark.rest.webshop.service.rest_webshop_service.ex;

public class ResourceAlreadyExists extends Exception {
    public ResourceAlreadyExists(String message) {
        super(message);
    }
}
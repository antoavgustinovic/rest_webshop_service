package com.ingemark.rest.webshop.service.rest_webshop_service.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(max = 10)
    private String code;

    private String name;

    @Min(0)
    private BigDecimal priceHrk;
    private String description;
    private boolean isAvailable;

    public Product() {
    }

    public Product(String code, String name, BigDecimal priceHrk, String description, boolean isAvailable) {
        this.code = code;
        this.name = name;
        this.priceHrk = priceHrk;
        this.description = description;
        this.isAvailable = isAvailable;
    }

    public Product(Long id, String code, String name, BigDecimal priceHrk, String description, boolean isAvailable) {
        this(code, name, priceHrk, description, isAvailable);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPriceHrk() {
        return priceHrk;
    }

    public void setPriceHrk(BigDecimal price_hrk) {
        this.priceHrk = price_hrk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price_hrk=" + priceHrk +
                ", description='" + description + '\'' +
                ", availability=" + isAvailable +
                '}';
    }
}

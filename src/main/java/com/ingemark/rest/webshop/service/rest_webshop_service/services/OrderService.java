package com.ingemark.rest.webshop.service.rest_webshop_service.services;

import com.ingemark.rest.webshop.service.rest_webshop_service.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    public List fetchOrdersFromCustomer(){
        return orderRepository.fetchOrdersFromCustomer();
    }
}

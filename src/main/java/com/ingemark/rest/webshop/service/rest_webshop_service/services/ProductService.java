package com.ingemark.rest.webshop.service.rest_webshop_service.services;

import com.ingemark.rest.webshop.service.rest_webshop_service.ex.ResourceAlreadyExists;
import com.ingemark.rest.webshop.service.rest_webshop_service.ex.ResourceNotFound;
import com.ingemark.rest.webshop.service.rest_webshop_service.model.Product;
import com.ingemark.rest.webshop.service.rest_webshop_service.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll()
                .forEach(products::add);
        return products;
    }

    public Product getProduct(Long id) throws ResourceNotFound {
        return productRepository.findById(id).orElseThrow(ResourceNotFound.supplier(id));
    }

    public void addProduct(Product product) throws Exception {
        if (productRepository.existsById(product.getId())) {
            throw new ResourceAlreadyExists(String.format("%s already exists", product));
        }
        productRepository.save(product);
    }

    public void updateProduct(Product product) throws Exception {
        getProduct(product.getId());
        productRepository.save(product);
    }
    public void deleteProduct(Long id) throws Exception {
        getProduct(id);
        productRepository.deleteById(id);
    }

}

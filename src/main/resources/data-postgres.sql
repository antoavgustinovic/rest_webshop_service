INSERT INTO product(id,code, name, price_hrk, description, is_available)
VALUES ('100001', 'as12frf123', 'Hrenovke', '12', 'Hrenovke najbolje kvalitete', true);
INSERT INTO product(id,code, name, price_hrk, description, is_available)
VALUES ('100002', 'as12frf124', 'Zelena salata', '3', 'Zelena salata iz domaćih polja', true);
INSERT INTO product(id,code, name, price_hrk, description, is_available)
VALUES ('100003', 'as12frf125', 'Jaja', '12', '10 komada najbolje kvalitete', true);
INSERT INTO product(id,code, name, price_hrk, description, is_available)
VALUES ('100004', 'as12frf126', 'Sava gume', '1200', 'Ljetne gume', false);
INSERT INTO product(id,code, name, price_hrk, description, is_available)
VALUES ('100005', 'as12frf127', 'Sprej za komarce', '15', 'Vrhunske kvalitete i djeluje odmah', false);

INSERT INTO customer(id, first_name, last_name, email)
VALUES ('20001', 'Luka', 'Kukec', 'luka.kukec@gmail.com');
INSERT INTO customer(id, first_name, last_name, email)
VALUES ('20002', 'Dinko', 'Marinac', 'dinko.marinac@gmail.com');
INSERT INTO customer(id, first_name, last_name, email)
VALUES ('20003', 'Josip', 'Horvat', 'josip.horvat@gmail.com');

INSERT INTO "order"(id, customer_id, total_price_hrk, total_price_eur, status)
VALUES ('30001', '20001', '1000', '150', 'SUBMITTED');
INSERT INTO "order"(id, customer_id, total_price_hrk, total_price_eur, status)
VALUES ('30002', '20001', '2100', '300', 'DRAFT');
INSERT INTO "order"(id, customer_id, total_price_hrk, total_price_eur, status)
VALUES ('30003', '20002', '700', '100', 'SUBMITTED');
INSERT INTO "order"(id, customer_id, total_price_hrk, total_price_eur, status)
VALUES ('30004', '20003', '7750', '1000', 'DRAFT')
DROP TABLE IF EXISTS product;
CREATE TABLE product
(
    id SERIAL NOT NULL,
    code VARCHAR(10) NOT NULL UNIQUE,
    name VARCHAR(100) NOT NULL,
    price_hrk NUMERIC(10,2) NOT NULL,
    description VARCHAR(255) NOT NULL,
    is_available BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS customer;
CREATE TABLE customer
(
    id SERIAL NOT NULL,
    first_name VARCHAR(10) NOT NULL ,
    last_name VARCHAR(100) NOT NULL ,
    email VARCHAR(100) NOT NULL ,
    PRIMARY KEY (id)
);

DROP TYPE IF EXISTS STATUS_T CASCADE;
CREATE TYPE STATUS_T as enum('DRAFT', 'SUBMITTED');

DROP TABLE IF EXISTS "order";
CREATE TABLE "order"
(
    id SERIAL NOT NULL,
    customer_id INTEGER,
    total_price_hrk NUMERIC(15,2) NOT NULL ,
    total_price_eur NUMERIC(15,2) NOT NULL ,
    status STATUS_T default 'DRAFT',
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS orderItem;
CREATE TABLE orderItem
(
    order_id INTEGER,
    product_id INTEGER,
    quantity INTEGER
)


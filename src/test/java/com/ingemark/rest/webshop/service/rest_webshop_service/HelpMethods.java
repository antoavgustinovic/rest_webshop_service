package com.ingemark.rest.webshop.service.rest_webshop_service;

import com.ingemark.rest.webshop.service.rest_webshop_service.model.Product;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class HelpMethods {

    public static void dataVerification(Product product, String code, String name, BigDecimal price, String description, boolean isAvailable) {
        assertThat(product.getCode()).isEqualTo(code);
        assertThat(product.getName()).isEqualTo(name);
        assertThat(product.getPriceHrk()).isEqualTo(price);
        assertThat(product.getDescription()).isEqualTo(description);
        if (isAvailable) {
            assertThat(product.getIsAvailable()).isTrue();
        } else {
            assertThat(product.getIsAvailable()).isFalse();
        }
    }

    public static Product productCackalice(){
        return new Product("as12frf129", "Čačkalice", new BigDecimal("11.99"),
                "500 komada", true);
    }
    public static Product productZelenaSalata(){
        return  new Product("as12frf124", "Zelena salata", new BigDecimal("2.49"),
                "Zelena salata iz domaćih polja", true);
    }
    public static Product productSprejZaKomarce(){
        return  new Product("as12frf125", "Sprej za komarce", new BigDecimal("14.99"),
                "Vrhunske kvalitete i djeluje instant", false);
    }
    public static Product productRiza(){
        return  new Product("as12frf128", "Riža", new BigDecimal("9.49"),
                "Riža iz najčišćeg mora", true);
    }
    public static Product productPempas(){
        return  new Product( 1L,"as12frf127", "Pempas",
                new BigDecimal("29.99"), "Za kvalitetnu kakicu", true);
    }

    public static Product productPempasNegPrice(){
        return  new Product( 1L,"as12frf127", "Pempas",
                new BigDecimal("-29.99"), "Za kvalitetnu kakicu", true);
    }

    public static Product productHrenovke(){
        return  new Product("as12frf123", "Hrenovke", new BigDecimal("14.99"),
                "Domaće hrenovke iy Slavonije", true);
    }
    public static Product productKikiriki(){
        return  new Product(1L, "as12frf111", "Kikiriki",
                new BigDecimal("49.99"), "U rinfuzi 1kg", true);
    }


}

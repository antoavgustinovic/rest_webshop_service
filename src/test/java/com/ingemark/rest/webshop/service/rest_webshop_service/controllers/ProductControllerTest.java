package com.ingemark.rest.webshop.service.rest_webshop_service.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingemark.rest.webshop.service.rest_webshop_service.HelpMethods;
import com.ingemark.rest.webshop.service.rest_webshop_service.model.Product;
import com.ingemark.rest.webshop.service.rest_webshop_service.repository.ProductRepository;
import com.ingemark.rest.webshop.service.rest_webshop_service.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.ingemark.rest.webshop.service.rest_webshop_service.HelpMethods.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ProductControllerTest {

    private MockMvc mockMvc;
    @Autowired
    protected WebApplicationContext context;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        productRepository.deleteAll();
    }

    @Test
    public void should_be_able_to_get_all_products() throws Exception {
        //given
        productRepository.save(productCackalice());
        productRepository.save(productZelenaSalata());
        productRepository.save(productSprejZaKomarce());

        //when
        MvcResult mvcResult = mockMvc.perform(get("/products")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        //then
        List<Product> products = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<List<Product>>() {
                });


        Product producT = products.get(0);
        HelpMethods.dataVerification(producT, "as12frf129", "Čačkalice", new BigDecimal(String.valueOf(11.99)),
                "500 komada", true);

        Product producT1 = products.get(1);
        HelpMethods.dataVerification(producT1, "as12frf124", "Zelena salata", new BigDecimal(String.valueOf(2.49)),
                "Zelena salata iz domaćih polja", true);

        Product producT2 = products.get(2);
        HelpMethods.dataVerification(producT2, "as12frf125", "Sprej za komarce", new BigDecimal(String.valueOf(14.99)),
                "Vrhunske kvalitete i djeluje instant", false);


    }

    @Test
    public void should_be_able_to_get_product() throws Exception {
        Product product = productRiza();
        productRepository.save(product);

        MvcResult mvcResult = mockMvc.perform(get("/products/" + product.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Product producT = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<Product>() {
        });

        HelpMethods.dataVerification(producT, "as12frf128", "Riža", new BigDecimal(String.valueOf(9.49)),
                "Riža iz najčišćeg mora", true);
    }

    @Test
    public void should_return_404_not_found_when_no_product_available() throws Exception {
        mockMvc.perform(get("/products/" + -1))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_be_able_to_create_new_product() throws Exception {
        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productPempas())))
                .andExpect(status().isOk());

        List<Product> products = productService.getAllProducts();
        Product product1 = productService.getProduct(products.get(0).getId());
        HelpMethods.dataVerification(product1, "as12frf127", "Pempas", new BigDecimal(String.valueOf(29.99)),
                "Za kvalitetnu kakicu", true);

    }

    @Test
    public void should_return_400_bad_request_if_product_already_exists() throws Exception {
        Product product = productSprejZaKomarce();
        productRepository.save(product);

        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(product)))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void should_return_400_bad_request_if_price_is_less_then_zero() throws Exception {
        Product product = productPempasNegPrice();

        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(product)))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void should_be_able_to_update_product() throws Exception {
        Product product = productHrenovke();
        productRepository.save(product);

        Product updatedProduct = new Product(product.getId(), "as12frf123", "Hrenovke",
                new BigDecimal("19.99"), "Domaće hrenovke izvrsne kvalitet kvalitete", true);

        mockMvc.perform(
                put("/products/" + product.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedProduct)))
                .andExpect(status().isOk())
                .andDo(print());

        Product product1 = productService.getProduct(product.getId());
        HelpMethods.dataVerification(product1, "as12frf123", "Hrenovke", new BigDecimal(String.valueOf(19.99)),
                "Domaće hrenovke izvrsne kvalitet kvalitete", true);
    }
    @Test
    public void should_return_400_bad_request_if_price_is_less_then_zero_when_updating_existing_product() throws Exception {
        Product product = productKikiriki();
        productRepository.save(product);

        Product updatedProduct = new Product(product.getId(), "as12frf111", "Kikiriki",
                new BigDecimal("-49.99"), "U rinfuzi 1kg", true);
        mockMvc.perform(put("/products/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedProduct)))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void should_return_404_not_found_when_updating_non_existing_product() throws Exception {

        Product updatedProduct = productKikiriki();
        mockMvc.perform(put("/products/" + 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedProduct)))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void should_be_able_to_delete_product() throws Exception {
        Product product = productSprejZaKomarce();
        productRepository.save(product);

        mockMvc.perform(delete("/products/" + product.getId()))
                .andExpect(status().isOk())
                .andDo(print());

        assertThat(productRepository.existsById(1L)).isFalse();
    }

    @Test
    public void should_return_404_not_found_when_deleting_non_existing_product() throws Exception {
        mockMvc.perform(delete("/products/" + 1324234))
                .andExpect(status().isNotFound())
                .andDo(print());
    }



}